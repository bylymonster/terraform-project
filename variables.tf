variable "AWS_DEFAULT_REGION" {
  type        = string
  description = "Região onde os recursos serão provisionados, variável mantida pelo Terraform Cloud"
}

variable "AWS_SECRET_ACCESS_KEY" {
  type        = string
  description = "Secret key da conta que sera provisionado o ambiente, variável mantida pelo Terraform Cloud"
}

variable "AWS_ACCESS_KEY_ID" {
  type        = string
  description = "Access key da conta que sera provisionado o ambiente, variável mantida pelo Terraform Cloud"
}

variable "DB_NAME" {
  type        = string
  description = "Nome do DynamoDB á ser provisonado"
}

variable "LAMBDA_POLICY_NAME" {
  type        = string
  description = "Nome da policy que o lambda utilizará"
}


variable "LAMBDA_FUNCTION_NAME" {
  type        = string
  description = "Nome da function lambda"
}


variable "CLOUDWATCH_EVENT_RULE_NAME" {
  type        = string
  description = "Nome da role do cloudwatch"
}
